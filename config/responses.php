<?php

return [
  'order' => [
    'get' => [
      'error_code' => null,
      'message' => 'Successfully get order(s).'
    ],
    'create' => [
      'error_code' => null,
      'message' => 'Order created successfully.'
    ],
    'update' => [
      'error_code' => null,
      'message' => 'Order updated successfully.'
    ],
    'delete' => [
      'error_code' => null,
      'message' => 'Order deleted successfully.'
    ]
  ],
  'category' => [
    'get' => [
      'error_code' => null,
      'message' => 'Successfully get category(s).'
    ],
    'create' => [
      'error_code' => null,
      'message' => 'Category created successfully.'
    ],
    'update' => [
      'error_code' => null,
      'message' => 'Category updated successfully.'
    ],
    'delete' => [
      'error_code' => null,
      'message' => 'Category deleted successfully.'
    ]
  ],
  'menu' => [
    'get' => [
      'error_code' => null,
      'message' => 'Successfully get menu(s).'
    ],
    'create' => [
      'error_code' => null,
      'message' => 'Menu created successfully.'
    ],
    'update' => [
      'error_code' => null,
      'message' => 'Menu updated successfully.'
    ],
    'delete' => [
      'error_code' => null,
      'message' => 'Menu deleted successfully.'
    ]
  ],
  'user' => [
    'get' => [
      'error_code' => null,
      'message' => 'Successfully get user(s).'
    ],
    'create' => [
      'error_code' => null,
      'message' => 'User created successfully.'
    ],
    'update' => [
      'error_code' => null,
      'message' => 'User updated successfully.'
    ],
    'delete' => [
      'error_code' => null,
      'message' => 'User deleted successfully.'
    ],
    'login' => [
      'error_code' => null,
      'message' => 'User login successfully.'
    ],
    'login_failed' => [
      'error_code' => 1000,
      'message' => 'Login failed'
    ],
    'register' => [
      'error_code' => null,
      'message' => 'User registered successfully.'
    ],
    'logout' => [
      'error_code' => null,
      'message' => 'User logout successfully.'
    ],
    'change_password' => [
      'error_code' => null,
      'message' => 'User changed password successfully.'
    ]
  ]
];
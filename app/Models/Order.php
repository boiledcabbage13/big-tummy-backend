<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    use HasFactory, SoftDeletes;

    protected $appends = ["total_price"];

    protected $fillable = [
        'transaction_id',
        'order_status',
        'discount',
        'note',
        'status'
    ];

    public function order_menus() {
        return $this->hasMany(OrderMenu::class);
    }

    public function getTotalPriceAttribute() {
        $data = $this->order_menus()->get();
        return $data->reduce(function ($carry, $item) {
            return $carry + ($item->quantity * $item->price); 
        }, 0);
    }
}

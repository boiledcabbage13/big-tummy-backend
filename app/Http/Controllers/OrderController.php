<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\OrderMenu;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $limit = $request->input('limit', null);
        $from = $request->input('from', null);
        $to = $request->input('to', null);
        $orderBy = $request->input('order_by', 'DESC');

        // $data = Order::with([
        //     'order_menus'
        // ]);

        $data = new Order();

        $data = $data
            ->when($from && $to, function($query) use ($from, $to){
                return $query->whereBetween('created_at', ["$from 00:00:00", "$to 23:59:59"]);
            })
            ->when($orderBy, function($query, $orderBy) {
                return $query->orderBy('created_at', $orderBy);
            });

        if($limit) {
            $data = $data->paginate($limit)->appends($request->all());
        } else {
            $data = $data->get();
        }

        // $data = $data->appends($request);

        return response()->json([
            'data' => $data,
            'error_code' => config('responses.order.get.error_code'),
            'message' => config('responses.order.get.message')
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $order = Order::create($request->all());

        foreach ($request->orders as $key => $value) {
            $create = [
                'order_id' => $order->id,
                'menu_id' => $value['id'],
                'quantity' => $value['quantity'],
                'price' => $value['price']
            ];

            OrderMenu::create($create);
        }

        return response()->json([
            'data' => $order->with('order_menus')->first(),
            'error_code' => config('responses.order.create.error_code'),
            'message' => config('responses.order.create.message')
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        return response()->json([
            'data' => $order->with('order_menus')->find($order->id),
            'error_code' => config('responses.order.get.error_code'),
            'message' => config('responses.order.get.message')
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        $order->fill($request->all());

        $order->save();

        OrderMenu::where('order_id', $order->id)->delete();

        foreach ($request->orders as $key => $value) {
            $create = [
                'order_id' => $order->id,
                'menu_id' => $value['menu_id'],
                'quantity' => $value['quantity'],
                'price' => $value['price']
            ];

            OrderMenu::create($create);
        }

        return response()->json([
            'data' => $order,
            'error_code' => config('responses.order.update.error_code'),
            'message' => config('responses.order.update.message')
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        $order->delete();

        return response()->json([
            'data' => null,
            'error_code' => config('responses.order.delete.error_code'),
            'message' => config('responses.order.delete.message')
        ]);
    }

    /**
     * Get total in specific date range.
     * 
     * @return \Illuminate\Http\Response
     */
    public function getTotal(Request $request) {
        $from = $request->input('from', null);
        $to = $request->input('to', null);

        $data = OrderMenu::when($from && $to, function($query) use ($from, $to){
            return $query->whereBetween('created_at', ["$from 00:00:00", "$to 23:59:59"]);
        })->get();

        $total = $data->reduce(function($carry, $item) {
            return $carry + ($item->quantity * $item->price);
        }, 0);

        return response()->json([
            'data' => [
                'total' => $total
            ],
            'error_code' => null,
            'message' => "Successfully get total amount of orders."
        ]);
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SignInController extends Controller
{
    public function login(Request $request) {
        $authenticate = Auth::attempt([
            'email' => $request->email,
            'password' => $request->password
        ]);

        if($authenticate){
            $user = Auth::user();

            $user->token =  $user->createToken('BigTummy')->accessToken;

            return response()->json([
                'data' => $user,
                'error_code' => config('responses.user.login.error_code'),
                'message' => config('responses.user.login.message')
            ]);
        }

        return response()->json([
            'data' => null,
            'error_code' => config('responses.user.login_failed.error_code'),
            'message' => config('responses.user.login_failed.message')
        ]);
    }

    public function logout() {
        Auth::guard('api')->user()->token()->delete();

        return response()->json([
            'data' => null,
            'error_code' => config('responses.user.logout.error_code'),
            'message' => config('responses.user.logout.message')
        ]);
    }

    public function register(Request $request) {
        $user = User::create($request->all());

        return response()->json([
            'data' => $user,
            'error_code' => config('responses.user.register.error_code'),
            'message' => config('responses.user.register.message')
        ]);
    }

    public function forgotPassword(Request $request) {
        $user = User::find(Auth::id());
        $user->password = $request->password;
        $user->save();

        return response()->json([
            'data' => $user,
            'error_code' => config('responses.user.change_password.error_code'),
            'message' => config('responses.user.change_password.message')
        ]);
    }

}

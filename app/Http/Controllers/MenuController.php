<?php

namespace App\Http\Controllers;

use App\Models\Menu;
use Illuminate\Http\Request;

class MenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $limit = $request->input('limit', null);
        $category_id = $request->input('category_id', null);
        $search = $request->input('q', null);

        $menus = Menu::with('category')
        ->when($category_id, function($query) use ($category_id) {
            return $query->where('category_id', $category_id);
        })
        ->when($search, function($query) use ($search) {
            return $query->where('name', "LIKE", "%{$search}%");
        });

        if($limit) {
            $menus = $menus->paginate($limit);
        } else {
            $menus = $menus->get();
        }

        return response()->json([
            'data' => $menus,
            'error_code' => config('responses.menu.get.error_code'),
            'message' => config('responses.menu.get.message')
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $menu = Menu::create([
            'name' => $request->name,
            'category_id' => $request->category_id,
            'price' => $request->price,
            'description' => $request->description
        ]);

        return response()->json([
            'data' => $menu,
            'error_code' => config('responses.menu.create.error_code'),
            'message' => config('responses.menu.create.message')
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function show(Menu $menu)
    {
        return response()->json([
            'data' => $menu,
            'error_code' => config('responses.menu.get.error_code'),
            'message' => config('responses.menu.get.message')
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Menu $menu)
    {
        $menu->fill($request->all());

        $menu->save();

        return response()->json([
            'data' => $menu,
            'error_code' => config('responses.menu.update.error_code'),
            'message' => config('responses.menu.update.message')
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function destroy(Menu $menu)
    {
        $menu->delete();

        return response()->json([
            'data' => null,
            'error_code' => config('responses.menu.delete.error_code'),
            'message' => config('responses.menu.delete.message')
        ]);
    }
}

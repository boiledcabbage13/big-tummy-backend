<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $limit = $request->input('limit', null);
        $search = $request->input('q', null);

        $categories = Category::when($search, function($query) use ($search) {
            return $query->where('name', "LIKE", "%{$search}%");
        });

        if($limit) {
            $categories = $categories->paginate($limit);
        } else {
            $categories = $categories->get();
        }

        return response()->json([
            'data' => $categories,
            'error_code' => config('responses.category.get.error_code'),
            'message' => config('responses.category.get.message')
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $category = Category::create([
            'name' => $request->name
        ]);

        return response()->json([
            'data' => $category,
            'error_code' => config('responses.category.create.error_code'),
            'message' => config('responses.category.create.message')
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        return response()->json([
            'data' => $category,
            'error_code' => config('responses.category.get.error_code'),
            'message' => config('responses.category.get.message')
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        $category->name = $request->name;

        $category->save();

        return response()->json([
            'data' => $category,
            'error_code' => config('responses.category.update.error_code'),
            'message' => config('responses.category.update.message')
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        $category->delete();

        return response()->json([
            'data' => null,
            'error_code' => config('responses.category.delete.error_code'),
            'message' => config('responses.category.delete.message')
        ]);
    }
}

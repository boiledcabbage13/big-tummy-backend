<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get("/test", function() {
    return 123;
});

Route::prefix('v1')->group(function () {
    Route::post('/login', 'SignInController@login');
    Route::post('/register', 'SignInController@register');
    Route::resource('/categories', 'CategoryController');
    Route::resource('/menus', 'MenuController');
    Route::prefix('/orders')->group(function() {
        Route::get('/total', 'OrderController@getTotal');
        Route::resource('/', 'OrderController');
    });
    Route::group(['middleware' => 'auth:api'], function(){
        Route::post('/logout', 'SignInController@logout');
        Route::post('/forgot-password', 'SignInController@forgotPassword');
    });
});
